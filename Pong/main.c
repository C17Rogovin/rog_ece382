#include <msp430.h> 
#include "pong.h"

/*
 * main.c
 * Author: Max Rogovin
 * Date: 8 Oct 15
 * Description: Main file of a Pong Game
 *
 * How did you verify your code functions correctly?
 * The debug and variable tracking tool is amazing
 *How could you make the "collision detection" helper functions only visible to your implementation file (i.e. your main.c could not call those functions directly)?
 * Have the functions called in the moveBall function.  Which I did.
 */


int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	
    ball alphaBall = createBall(0,0,5,5,0x2);
    alphaBall = moveBall(alphaBall);

	return 0;
}
