;-------------------------------------------------------------------------------
;           Coded by: C2C Max B Rogovin, CS-36
;           Program: Lab 2, Crypto
;           Due: __ Sept 2015
;           Documentation:
;-------------------------------------------------------------------------------
            .cdecls C, LIST, "msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
                                            ; Assemble into program memory
            .data
Plaintext:  .space 100                      ; space in memory for results of program

            .text                           ; Assemble into program memory
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section
            .retainrefs                     ; Additionally retain any sections
                                            ; that have references to current
                                            ; section
CIPHERTEXT: .byte 0xef,0xc3,0xc2,0xcb,0xde,0xcd,0xd8,0xd9,0xc0,0xcd,0xd8,0xc5,0xc3,0xc2,0xdf,0x8d,0x8c,0x8c,0xf5,0xc3,0xd9,0x8c,0xc8,0xc9,0xcf,0xde,0xd5,0xdc,0xd8,0xc9,0xc8,0x8c,0xd8,0xc4,0xc9,0x8c,0xe9,0xef,0xe9,0x9f,0x94,0x9e,0x8c,0xc4,0xc5,0xc8,0xc8,0xc9,0xc2,0x8c,0xc1,0xc9,0xdf,0xdf,0xcd,0xcb,0xc9,0x8c,0xcd,0xc2,0xc8,0x8c,0xcd,0xcf,0xc4,0xc5,0xc9,0xda,0xc9,0xc8,0x8c,0xde,0xc9,0xdd,0xd9,0xc5,0xde,0xc9,0xc8,0x8c,0xca,0xd9,0xc2,0xcf,0xd8,0xc5,0xc3,0xc2,0xcd,0xc0,0xc5,0xd8,0xd5,0x8f
KEY:        .byte 0xac
LENGTH:     .equ 1    ; in bytes, of key
MESLEN:     .equ 94   ; length of message
ZERO:       .equ 0x00
TOP:        .equ 0x200


;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

;-------------------------------------------------------------------------------
            mov.w #KEY, r4        ; store key loc in mem
            mov.w #CIPHERTEXT, r5 ; store ciphertext loc in mem
            mov.w #MESLEN, r6     ; i need the length of the word
            mov.w #ZERO, r7       ; length of key

            mov.w #Plaintext, r14 ; move address to store result into register

NotDoneYet: mov.b @r5+, r12       ; move ciphertext byte into r12, and postincriment r5 for next loops

            mov.b @r4, r13        ; move the key byte into r13

            inc r4                ; inc location of key to next bit
            inc r7                ; inc length
            cmp #LENGTH, r7
            jz ResetKey

DecryRes:   call #DecryptByte
            nop
            dec  r6               ; we moved a bit along, there is only so much length left

            cmp #ZERO, r6         ; check to see if we have run out of bytes to decrypt
            jnz NotDoneYet

ResetKey:   sub.w #LENGTH, r7
            sub.w #LENGTH, r4
            jmp DecryRes

forever:    jmp forever
;---------------------------------------------------
;Subroutine Name: DecryptByte
;Authoer: C2C Max Rogovin
;Function: XORs a byte with the key, returns the plaintext result
;Inputs: address of ciphertext byte in r12, address of the key byte in r13, address of output as r14
;Outputs: result in r13, written into mem at the address held in r14
;Registers destroyed: r13
;---------------------------------------------------
DecryptByte:
            XOR.b r12, r13     ; XOR decrypt
            mov.b r13, 0(r14)  ; store in RAM
            inc r14          ; incriment to write next byte
            ret

;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect 	.stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
