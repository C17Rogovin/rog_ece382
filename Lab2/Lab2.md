# Lab 2:  Attack of the Crypto #

## Objectives and purpose ##

The objective of the lab is to program a microcontroller to perform simple cryptographic algorithm.  The purpose of the lab is to practice writing some subroutines, using both the call-by-value and call-by-reference techniques to pass arguments to the subroutines.

## Prelab ##

The purpose of the prelab was to build a flowchart to demonstrate the use of subroutines in the decryption of a given ciphertext.  The flowchart is shown below:

![Prelab](images/flowchart.PNG)

## Preliminary design ##

The cryptography method used in this lab is the XOR encrypt/decrypt.  This is used because the same key can be used to encode and then decode the plaintext message and ciphertext.  In the case of a binary system, the message is represented by ASCII characters.  ASCII varies in range, from 0x00, to 0xFF.  However, only certain values are actually English characters or number characters.  

Preliminary steps for designing a decryption algorithm involved a step-by-step approach.  Building from basic functionality to more expanded functionality was a better approach in this case.

The first step was to get the required functionality working properly.  The ciphertext and length of the key are known.  The required functionality involved a key of a single byte to decrypt the ciphertext.  This assumes that every byte is properly decrypted by the key.  Thus, the program should loop through each byte of the ciphertext and XOR the ciphertext with the key.  After each byte is decrypted, the results are stored in RAM.

For B functionality, the length of the key is still known, though it could be multiple bytes.  The trick here is to keep looping through the ciphertext, using the next byte of the key.  Once the program has looped through the key, and there is still ciphertext bytes to decrypt, the program resets to the beginning of the key and keeps looping through the ciphertext. After each byte is decrypted, they are stored in RAM.   

For A functionality, a different approach was needed.  Because no information about the key, either length or contents, were known.  In this case, a brute-forcing method for determining the key, and contents of the message, is needed.To brute-force, you essentially guess every possible combination until the message is decoded.  To do this, I would establish a "guess byte".  This byte would be constantly incremented until the byte successfully decrypts a byte.  The message then stores this guess byte in memory until either the program successfully decrypts the message, or an invalid ASCII character is detected.  If an invalid ASCII character is detected, the program will revert a guess byte and continue to incriment.  If the guess byte reaches 0xFF, the program will revert two guess bytes and continue to increment.  This is a very computationally-intensive operation for a small CPU, and could take a very large amount of time. 

## Software flow chart / algorithms ##


## Well-formatted code ##

![code1](images/code1.PNG)
![code2](images/code2.PNG)
![code3](images/code3.PNG)

## Debugging and Testing Methodology ##

To successfully debug, the program had to read RAM in character mode (which reads ASCII values).  Otherwise, using standard internet hex-to-ASCII converters available on the internet, there might be some issues.  Namely, that MSP430 uses little-endian memory format.  

For required and B functionality, I did not need to make additional test cases. They were not computationally intensive, and were ciphertext of an actual message.  It would be hard to build a test case without already having a functioning decryption function, as decryption and encryption in this case are the same computational function and algorithm.

To test, I simply used the functionality test cases provided.  Initially, I counted down from the length of the key while cycling through the key.  However, this produced several errors, namely keeping track of the several variable associated with tracking the working location of what byte in the key the program is at.  Some results would blow past 0 and proceed into negative numbers, causing several errors with overflows.  The program would run endlessly into RAM, and not work properly past the first few letters no greater than the length of the key. 

I did make an attempt at the A functionality, but errors due to having created a backup .asm file (namely, CCS trying to compile two assembly files at the same time) stymied my attempts at coding the brute-force algorithm.  I talked to Capt. Falkinburg and fixed the error, without enough time to code the A functionality.  I did explain a method for brute-forcing, and I am certain that given enough time to code it, it would have worked properly.  

## Results ##

The results, when tested, produced messages.  These messages are shown below.

![Required_Functionality](images/bfxntest.PNG)
![B_Functionality](images/reqfxntest.PNG)

## Observations and Conclusions ##

In the end, the purpose of this lab was to become familiar with subroutines. The subroutine I used is in accordance with coding conventions.  It was reusable in multiple both required and B functionality without needed modification.  Because the rest of the code was about setting up the registers and memory to perform and record the results of the decryption, only that needed to be modified.  

Subroutines are very useful and could be beneficial to future programs and labs.    

### Documentation ###

EI w/ LTC Falkinburg.  