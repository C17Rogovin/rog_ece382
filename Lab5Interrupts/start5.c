//-----------------------------------------------------------------
// Name:	Rogovin
// File:	lab5.c interrupts
// Date:	Fall 2015
// Purp:	Demo the decoding of an IR packet and triggering an action based on that input
//-----------------------------------------------------------------
#include <msp430g2553.h>
#include "start5.h"

int8	newIrPacket = FALSE;
int16	packetData[34];   //size needed in testing, because catching start 1 and 0
int32   IRpacket = 0;     //actual recorded data 1 and 0 rather than length of pulse
int8	packetIndex = 0;  //init to 0
int16	count=0;

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void main(void) {

	initMSP430();				// Set up MSP to process IR and buttons

	while(1){
		if (packetIndex > 33) {    //check for full packet
			newIrPacket = TRUE;  //make sure flag is set
			IRpacket = 0;
			packetIndex = 0;

		} // end if new IR packet arrived
		if (newIrPacket){
			int i;
			for (i=2; i < 34; i++){  //index at 2 to avoid start 1 and 0.

				if ((packetData[i] > minLogic1Pulse) && (packetData[i] < maxLogic1Pulse)) { //check pulse width for logic 0
					IRpacket = IRpacket*2; //shift by multiplying to move bits
				}

				if ((packetData[i] > minLogic0Pulse) && (packetData[i] < maxLogic0Pulse)) {  //check pulse width for logic 1
					IRpacket = IRpacket*2+1; //set bit and shift to add in a 1 bit
				}

			}
			if(IRpacket == PWR){
				P1OUT ^= BIT6; //toggle red LEF
			}
			else if(IRpacket == OK){
				P1OUT ^= BIT0; //toggle green LED
			}

			else if(IRpacket == DOWN){
				P1OUT ^= (BIT0|BIT6); //toggle both
			}
			unsigned int wait = 0;
			for (wait = 0; wait < 65000; wait++){  //wait to give time to execute function above

			};
			newIrPacket = FALSE;  //reset flag
			packetIndex = 0;     //reset packet index
			IRpacket = 0;        //set value of packet to 0 (and avoid re-triggering)

		} // end if new IR packet arrived
	} // end infinite loop
} // end main

// -----------------------------------------------------------------------
// In order to decode IR packets, the MSP430 needs to be configured to
// tell time and generate interrupts on positive going edges.  The
// edge sensitivity is used to detect the first incoming IR packet.
// The P2.6 pin change ISR will then toggle the edge sensitivity of
// the interrupt in order to measure the times of the high and low
// pulses arriving from the IR decoder.
//
// The timer must be enabled so that we can tell how long the pulses
// last.  In some degenerate cases, we will need to generate a interrupt
// when the timer rolls over.  This will indicate the end of a packet
// and will be used to alert main that we have a new packet.
// -----------------------------------------------------------------------
void initMSP430() {

	WDTCTL=WDTPW+WDTHOLD; 		// stop WD

	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	P2SEL  &= ~BIT6;					// Setup P2.6 as GPIO not XIN
	P2SEL2 &= ~BIT6;
	P2DIR &= ~BIT6;

	P2IFG &= ~BIT6;						// Clear any interrupt flag on P2.3
	P2IE  |= BIT6;						// Enable P2.3 interrupt

	HIGH_2_LOW;							// check the header out.  P2IES changed.


	P1DIR |= BIT0 | BIT6;				// Enable updates to the LED
	P1OUT &= ~(BIT0 | BIT6);			// An turn the LED off

	TA0CCR0 = 0xffff;					// create a 16mS roll-over period
	TACTL &= ~TAIFG;					// clear flag before enabling interrupts = good practice
	TACTL = (ID_3 | TASSEL_2 | MC_1);   // Use 1:8 prescalar off SMCLK and enable interrupts
	TACTL &= ~TAIE;                     //clear interrupt flag

	_enable_interrupt();
}

// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------
#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {

	int8	pin;
	int16	pulseDuration;			// The timer is 16-bits

	if (IR_PIN)
		pin=1;
	else
		pin=0;

	switch (pin) {					// read the current pin level
		case 0:						// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!


			TACTL &= ~MC_1;
			TACTL &= ~TAIE;  //off timer
			pulseDuration = TAR;    //get pulse duration from the TAR
			packetData[packetIndex++] = pulseDuration;  //index along the packet data to record next logic bit length

			LOW_2_HIGH; // Set up pin interrupt on positive edge
			break;

		case 1:							// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TAR = 0x0000;						// time measurements are based at time 0
			TACTL |= MC_1;   //turn timerA on
			HIGH_2_LOW; 						// Set up pin interrupt on falling edge
			break;
	} // end switch

	P2IFG &= ~BIT6;			// Clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {

	TACTL &= ~MC_1;  //off interrupt
	TACTL &= ~TAIE;  //off timer

	newIrPacket = TRUE;  //flag set because data present
	packetIndex = 0;     //make sure you are recording from the top of the packet
	TACTL &= ~TAIFG; // clear flag
}
