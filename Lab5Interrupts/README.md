## Lab 5: The Interrupt Strikes Back ##

### Objectives ###

The Objectives of the lab were to:

* For required functionality: Use an interrupt to determine which buttons was pressed on a remote, and read the input to trigger or toggle LEDs.

### Purpose ###

The purpose of the lab was to learn how to properly use interrupts on a microcontroller and to respond to inputs.  In this case, the input is a IR remote.

## Prelab ##

### Characterizing the Remote ###

The first step in getting the program working was to characterize the input. In order to do this, the input had to be "characterized."  Essentially, the IR waveform had to be understood.  This involves finding out what a logic 1 and a logic 0 looks like, and other features of the input, such as start and stop bits.

After determining what constituted a logic 1 and a logic 0, I read the 32-bit pulse to see what data was transmitted.  Shown below is the output of 10 different buttons on a standard ______ remote (labeled DEFC-7). 

### Logic 1 versus Data 1###

The waveform does not actually directly give the logic inputs.  The waveform alerts the receiver to an incoming transmission by first showing a start 1 and 0.  These pulses is significantly longer than actual logic.  For the actual data, the zero pulsed for a constant amount of time, and the logic 1 or logic 0 was dictated by the length of the data 1.  This was measured using the code below:

![code](images/test5c.PNG)

For clarification, a data 1 or 0 refers to the state of the IR transmitter, while logic 1 or 0 refers to the pair of data half-bits.  Shown below is the waveform clarifying the bits.  

![example](images/exwaveform.PNG)

In order to determine which was a logic 1 and logic 0, the times of the pulses needed to be characterized.  10 samples each of logic 1, logic 0, and stop 1 bits were sampled.  In addition, their means and standard deviations were sampled, in order to provide margins of error (after all, a pulse will not always be 100% consistent!).  The table below shows the samples taken and means with standard deviations.  

![pulse_time](images/pulsetime.PNG)

I also sampled all half-pulses to get the number of milliseconds the pulse actually is, as well as how many "counts" on the TA0CCR0.  Table shown below:

![time_with_counts](images/timecounts.PNG)

## Required Functionality: Toggle the LEDs ##

### Setup ###

The first step to preparing the program to do what it needs to do is initialize the inputs and outputs.  The LEDs and input pin for the transistor sensor were set up and GPIO as needed.  Additionally, the interrupt flags and TA0CCR0 were set up as needed.  Shown below is the initialization code:

![setup](images/init.PNG)

### Interrupts ###

The interrupts were set up in such a manner that if the TA0CCR0 (timed such that the full count from 0 to the max value exceeds all 1 half-bits) exceeds the length of any final bits, the interrupt is triggered and the microcontroller will perform the task of comparing the recieved data with known values of remote transmissions to trigger an action.  Shown below is the known values used for the LEDs:

![header](images/header1.PNG)
![header](images/header2.PNG)

### Flagging ###

During the interrupt, the variable newIrPacket was used as a flag.  If the flag is set, a complete transmission has been received, and this flag is used to trigger an interpretation of the data pulse lengths into logic bits in IRpacket. Shown below is that code:  

![flag](images/flag.PNG)

If IRpacket matched the value for a button, its respective LED(s) were toggled.  Shown below:

![LED](images/LED.PNG)

## Debugging ##

### Bits shifted on input ###

My biggest problem was that my code was not reading the input properly.  I tried several things, which did involve a few problems with the interrupt (explained later).  However the biggest issue was the program not reading the data properly.  I tried several things.  The first was increasing the data initially read to above 32 bits.  For some reason, the first two received bits were very long, and thus not useful.  However, because my code measured the length and the extraneous (presumably start) pulses were not within specified limits, it shouldn't have done anything anyway.  This did not fix the problem, though I could see that the last 32 bits did match expected inputs, the program was not interpreting properly.  

I attempted to fix this by indexing the for loop for processing bits at 1 (to avoid the first 2 bits).  This did also not fix this problem.  I suspect the problem is either improper indexing or improper data handling.  Since I cannot pin down the problem, I used the program to read what the inputs were interpreted as when I actually did use the remote inputs, and set those as the constant that the button toggles were looking for in the comparisons.  

### Properly Interrupting ###

The other issue I had was with interrupts.  Some of the syntax and bit setting/clearing, and other issues.  To fix the issue, I referenced the provided image outline, shown below:

![outline](images/outline.jpg)

Among other issues, were improperly clearing, and using the &= to attempt to set rather than clear as a &= ~(BIT#).  This caused some issues.  Additionally, I was enabling and disabling interrupts in far too many places.  This was lagging the MSP430 and resetting far too many things.  Eliminating the excess code greatly assisted things.  With my program matching the layout suggested in the assignment document, my code functioned (with the hotfix).  

## Observations and Conclusions ##

This stuff is complex.  Lots of moving parts, and doing what is essentially turning time into bits.  Using the timer to do so AND trigger interrupts was a complex piece of code.  In the end, the use of a sensor input, once interpreted, is a very useful skill to have in the use of microcontrollers and other specific-function devices.  I imagine robotics function very similarly.

## Documentation ##

* EI W/ Capt Falkinburg: Troubleshooting and my general stupidity
