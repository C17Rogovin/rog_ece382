//-----------------------------------------------------------------
// Name:	Rogovin
// File:	lab5.h
// Date:	Fall 2105
// Purp:	Include file for the MSP430, header file for main projects
//-----------------------------------------------------------------
//-----------------------------------------------------------------
// Page 76 : MSP430 Optimizing C/C++ Compiler v 4.3 User's Guide
//-----------------------------------------------------------------
typedef		unsigned char		int8;
typedef		unsigned short		int16;
typedef		unsigned long		int32;
typedef		unsigned long long	int64;
#define		TRUE				1
#define		FALSE				0
//-----------------------------------------------------------------
// Function prototypes found in lab5.c
//-----------------------------------------------------------------
void initMSP430();
__interrupt void pinChange (void);
__interrupt void timerOverflow (void);
//-----------------------------------------------------------------
// Each PxIES bit selects the interrupt edge for the corresponding I/O pin.
//	Bit = 0: The PxIFGx flag is set with a low-to-high transition
//	Bit = 1: The PxIFGx flag is set with a high-to-low transition
//-----------------------------------------------------------------

#define		IR_PIN			(P2IN & BIT6)
#define		HIGH_2_LOW		P2IES |= BIT6
#define		LOW_2_HIGH		P2IES &= ~BIT6


#define		averageLogic0Pulse	0x0235
#define		averageLogic1Pulse	0x0672
#define		averageStartPulse	0x1100
#define		minLogic0Pulse		averageLogic0Pulse - 50
#define		maxLogic0Pulse		averageLogic0Pulse + 50
#define		minLogic1Pulse		averageLogic1Pulse - 50
#define		maxLogic1Pulse		averageLogic1Pulse + 50
#define		minStartPulse		averageStartPulse - 100
#define		maxStartPulse		averageStartPulse + 100

#define		PWR		0xCF2057A8    //not matching due to shifted by 2 bits- can't determine cause
#define		OK      0xCF203FC0
#define     DOWN    0xCF20E718

