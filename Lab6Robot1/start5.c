//-----------------------------------------------------------------
// Name:	Rogovin
// File:	lab6 A functionality
// Date:	Fall 2015
// Purp:	Move a robot using IR remote inputs

//Documentation: Got help from C2C Chiriac getting the interrupt code functioning properly
//-----------------------------------------------------------------
#include <msp430g2553.h>
#include "start5.h"
#include "Robot.h"
int8	newIrPacket = FALSE;
int16	packetData[32];
int8	packetIndex = 0;
int32   irPacket = 0;

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void main(void) {

	initMSP430();				// Set up MSP to process IR and buttons
	initRobot();                //init robot
	//aheadFull();

	while(1)  {  //always running while on

if (newIrPacket == TRUE){
	newIrPacket = FALSE;
	switch (irPacket){

					case CH_UP :
						aheadFull();
						//P1OUT |= BIT6; //Green on
						break;
					case VOL_UP:
						hardToStarboard();
						//P1OUT |= BIT0; //Red on
						break;
					case CH_DW :
						fullReverse();
						//P1OUT &= ~BIT6; // Green off
						break;
					case VOL_DW:
						hardToPort();
						//P1OUT &= ~BIT0; // Red off
						break;
					case TWO:
						driftToPort();
						//P1OUT |= BIT6;  //Green on
						//P1OUT |= BIT0;  //Red on
						break;
					case ONE:
						//P1OUT &= ~BIT6; // Green off
						//P1OUT &= ~BIT0; // Red off
						driftToStarboard();
						break;

				} //end switch

		} // end if new IR packet arrived

	} // end infinite loop
} // end main

// -----------------------------------------------------------------------
// In order to decode IR packets, the MSP430 needs to be configured to
// tell time and generate interrupts on positive going edges.  The
// edge sensitivity is used to detect the first incoming IR packet.
// The P2.6 pin change ISR will then toggle the edge sensitivity of
// the interrupt in order to measure the times of the high and low
// pulses arriving from the IR decoder.
//
// The timer must be enabled so that we can tell how long the pulses
// last.  In some degenerate cases, we will need to generate a interrupt
// when the timer rolls over.  This will indicate the end of a packet
// and will be used to alert main that we have a new packet.
// -----------------------------------------------------------------------
void initMSP430() {

	WDTCTL=WDTPW+WDTHOLD; 							// stop WD

	IFG1 = 0;  										//clear flag
	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	TAR = 0x0000;									// Fill in the next six lines of code.
	TA0CTL &= ~TAIFG;                    			// ensure TAR is clear TA0 specified for this code
	TA0CCR0 = 2500;								    // create a ~65 ms roll-over period with TA0CCR0
	TA0CTL |= ID_3 | TASSEL_2 | MC_1;				// Use 1:8 prescalar off SMCLK

	P2DIR &= ~BIT6;									// Set up P2.6 as GPIO not XIN
	P2SEL &= ~BIT6;									// This action takes
	P2SEL2 &= ~BIT6;								// three lines of code.

	P2IFG &= ~BIT6;									// Clear any interrupt flag on P2.3
	P2IE  |= BIT6;									// Enable P2.3 interrupt

	HIGH_2_LOW;										// check the header out.  P2IES changed.

	P1DIR |= BIT0 | BIT6;							// Set LEDs as outputs
	P1OUT &= ~(BIT0 | BIT6); 						// And turn the LEDs off

	_enable_interrupt();
}

// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------
#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {

	int8	pin;
	int16	pulseDuration;			// The timer is 16-bits

	if (IR_PIN)		pin=1;	else pin=0;

	switch (pin) {					// read the current pin level
		case 0:						// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
			pulseDuration = TAR;

			irPacket <<= 1;
			if (pulseDuration > minLogic1Pulse){
				irPacket +=1;		// 1
			}
			packetData[packetIndex++] = pulseDuration;
			LOW_2_HIGH; 				// Set up pin interrupt on positive edge
			break;

		case 1:							// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TAR = 0x0000;						// time measurements are based at time 0
			HIGH_2_LOW; 						// Set up pin interrupt on falling edge
			TA0CTL |= TAIE; 						// en timer A interrupt
			break;
	} // end switch

	P2IFG &= ~BIT6;			// Clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR


#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {

	TA0CTL &= ~TAIE;			//disen TimerA
	packetIndex = 0;
	newIrPacket = TRUE;
	TA0CTL &= ~TAIFG; //clear flag before en interrupt// GOOD PRACTICE
}
