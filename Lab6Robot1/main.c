#include <msp430.h>
#include "Robot.h"

void main(void){
	initRobot();
	while (1) {
    	aheadFull();
    	fullReverse();
    	hardToPort();
    	hardToStarboard();
    	driftToPort();
    	driftToStarboard();

    }
}

