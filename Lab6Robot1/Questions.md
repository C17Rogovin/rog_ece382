Q: Consider your hardware (timer subsystems, chip pinout, etc.) and how you will use it to achieve robot control. Which pins will output which signals you need? 

A: I’m going to output on P2.1, P2.2, P2.4 and P2.5 for the motors (PWM signal)

Q: Which side of the motor will you attach these signals to? 

A: P2.1/2 will be left, and P2.4/5 will be right.  Using the TA1.1 for the left side, and TA1.2 for the right side (TA1.0 is TA0CRR0, or the max count).

Q: How will you use these signals to achieve forward / back / left / right movement? 

A: Left and right moving the motors at the same speed is forward.  At no speed is stopped.  Moving one wheel while the other is stopped is a small turn, moving one faster than the other is a large-radius turn.  Backwards is inverting the signal on a forward movement.

Q:Consider how you will setup the PWM subsytem to achieve this control. What are the registers you'll need to use? 

A: I’m going to use the TA1CCR1 and TA1CCR2 for the left and right, respectively.  To keep things simple, TA1CRR0 will be 1000 (so 25% will be 250 on the registers).

Q: Which bits in those registers are important? What's the initialization sequence you'll need?

A: The capture and control registers will be important, as these will set the flags needed to do PWM to fire the motors.  I’ll need to use 2 (one for each motor).  I’ll initialize the timer, set the max value, the trigger values, and connect the pins to the flags as outputs.  I’ll also set the mode (3 or 7) as needed.  

Q: Consider what additional hardware you'll need (regulator, motor driver chip, decoupling capacitor, etc.) and how you'll configure / connect it.

A: Shown in the schematic.

Q: Consider the interface you'll want to create to your motors. Do you want to move each motor invidiually (moveLeftMotorForward())? Or do you want to move them together (moveRobotForward())?

A: Moving individually for small turns, larger turns might require a separate function or larger inputs.  Forward and backwards will require movement at the same time and magnitude.  