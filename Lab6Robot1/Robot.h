//-----------------------------------------------------------------
// Function prototypes found in main.c
//-----------------------------------------------------------------

void initRobot();
void aheadFull();
void driftToPort();
void hardToPort();
void driftToStarboard();
void hardToStarboard();
void leftReverse();
void rightReverse();
void leftFoward();
void rightFoward();
void fullReverse();
