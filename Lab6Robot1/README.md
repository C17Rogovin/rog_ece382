## Lab 6: Return of the Robot ##

### Objectives ###

The Objectives of the lab were to:

* For required functionality: Wire and code a two-wheeled robot to move forward and backwards, and to have the robot turn 90 degrees and less than 45 degrees both left and right.
* For A functionality, to use Lab 5 code to get the robot to move based on input commands from a remote.  

### Purpose ###

The purpose of the lab was to learn how to properly work with a robot and drive motors using Pulse Width Modulation (PWM) signals. 

## Prelab ##

The prelab's main purpose was to take an initial pass at thinking through the hardware and software design of the robot.  I won't exaggerate.  I had no clue what I was doing.  Most of the understanding I gained from this lab came during my 8-hour stint in the lab on Saturday.  The three main parts of the prelab were: A wiring diagram, a software flowchart, and a few questions to get me thinking.  

My first pass at the wiring diagram was as such:

![DRAFT](images/draft.PNG)

The first pass at the software flowchart was as such:

![FLOWCHART](images/flowchart.PNG)

And my responses to the questions are in a separate markdown file in this directory (Questions.MD).

The initial pass at wiring was deeply flawed, but I will discuss my refinement of my approach in the next section.

## Required Functionality: Move the Damn Thing ##

### Setup/wiring ###

Long story short, my initial wiring diagram was poor.  After some thought, refinement, and testing, I finalized on the following layout:

![Revised Wiring](images/final.PNG)

The big changes involve the elimination of the enable wiring (I found it did nothing, even when power was provided).  I also changed the wiring to properly line up with the TA1CCR1/2 outputs.  I also made sure the decoupling capacitors were placed properly.  Also, I properly wired the motors.  I also fixed the power system.  Now, with the proper voltages applied to the pins, the robot should move.

Shown below is the final product:

![Revised Wiring](images/wiring.jpg)

### Pulse Width Modulation: PWM ###

To actually drive the motors, a voltage differential between the motor terminals must be applied.  The power to actually turn the motors is mostly a function of current, supplied by the power supply (in this case, a battery).  

A small quirk of digital microcontrollers is that there are only two voltage settings:  on and off.  In the case of the MSP430, this is 0V or 3.3V.  Using the motor driver chip the voltages that could be delivered to the motor are either 0V or 12V.  

However, there is a small problem.  Using more than 1A of current to supply the motors with power presents dangers to the motor and motor driver chip.  Torque in the motors creates an AC signal which has the potential to exceed limits in the robot and fry components.  I used a motor provided for testing in order to see how much current a constant 12V power supply would draw.  To my delight and surprise, it was less than 1A, meaning I could power the motor all I wanted with the robot and not be in danger of drawing too much current.  

Pulse width modulation is simply keeping the output on for a certain percentage of time.  In order to do this with the MSP430, a register called the "capture and control" register must be used.  By setting TA1CRR0 to 1000, and TA1CCR1 and TA1CCR2 to whatever was needed to duty-cycle (values of the TA1CCR1/2 divided by TA1CRR0 times 100 equals the duty cycle as a percentage).  This was initialized in the registers, shown:

![LED](images/init.PNG)

By setting these values, and then setting the mode and the pins (P2.1/4 for forward and P2.2/5 for reverse), shown below, allows the robot to drive at a certain speed.  By setting the TA1CCR1 and TA1CCR2 at different speeds, turning could be achieved.  

![LED](images/turnex.PNG)

One last note: In order to supply a voltage differential to the motor and drive it, one pin for each motor must be on, and the other pin connected to that motor must be off.  By "grounding" the pin that is "off" by setting it to zero. Shown below: 

![LED](images/reverse1.PNG)
![LED](images/reverse2.PNG)

### Maneuver Modes ###

I chose to drive my motors independently.  By breaking down the functions for each motor into smaller functions, I afford myself more flexibility in future labs.

I accomplish moving forward by moving the motors at the same power setting (not the same as the duty cycle, as both motors are not the same.)  In order to move in reverse, I set each motors pins to the reverse setting separately, moved the robot, and then reset into the forward mode to keep things simple. I had to use different duty cycles due to differing strengths/inefficiencies of the motors to move straight. Shown:

![LED](images/forward.PNG)

For turns, I set the duty cycle of the motor on the side i want to turn to to 0%, and then engage the other motor.  If I wanted to, I could turn in place by engaging the turn-side motor in reverse.  I might do this in the future if I need to for future labs, and should be quick to do.  

Shown is an example of a turn:

![LED](images/turnex.PNG)

Finally, I use the delays like I do to avoid extra torque on the motors, so I don't accidentally burn out the motors with a huge spike in current.  It's safer to let the robot come to a stop than immediately try to engage another movement mode.  

Required functionality was just to demonstrate that these commands work, shown:

![LED](images/reqfnx.PNG)

## A Functionality: Remote Control ##

### Lab 5 contribution ###

Implementing the commands to the robot via IR remote was simple.  Because the code for Lab5 did not use any of the same pins or registers/clocks as the Lab6 code, I was simple able to use the Lab5 c and header files, import and initialize the robot functions, and then change the LED responses to robot functions.  

Please note that I used code from C2C Chiriac, as my code was buggy and was not suitably reliable to use in the robot.  This was documented and done with approval from C2C Chiriac.  

Changes in the code shown below:  

![LED](images/changes.PNG)

## Debugging ##

This was more difficult.  Most of the problems involved properly setting the pins to engage the reverse modes, and some shenanigans with P2OUT.  That was an easy fix (delete all references).  Figuring out other issues involved use of the oscilliscope.  By hooking up my board not on the robot and using the o-scope probes it was a lot easier than dealing with the mess of wiring.  It also allowed me to isolate issues to wiring or code, which was useful.

Floating grounds (fixed by replacing a faulty grounding wire) and power issues involving putting in the motor driver chip backwards were easy fixes once the problem was recognized.  In the end, it was a lot of little things and many hours needed to properly get the robot to function.

## Observations and Conclusions ##

Robots are interesting little devices.  There is a lot going on even for just basic functions and movement.  PWM, interrupts for inputs, and wiring/pin setting is a lot to keep track of and plan.  In the end, it was more thought than code.  Putting the time in up-front to think out the problem saves a LOT of time down the road. 

## Documentation ##

* EI W/ Capt Falkinburg: Troubleshooting and my general stupidity
* Spent most of my Saturday working with C2C Chiriac, we worked together on a wiring diagram and then on code and robot-related theory and implementation
