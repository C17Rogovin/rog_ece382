#include <msp430.h>

/*
 * pong.h
 * Author: Max Rogovin
 * Date: 8 Oct 15
 * Description: header file for a pong game setup
 */
#ifndef _PONG_H
#define _PONG_H

#define SCREEN_X 230
#define SCREEN_Y 310
#define NEG_ONE -1

typedef struct{
	int x;
	int y;
}vector;

typedef struct{
	vector position;
	vector velocity;
	unsigned char radius;
}ball;


ball createBall(int startX, int startY, int XballVelocity, int YballVelocity, unsigned char radius);

ball moveBall(ball movingBall);

#endif

