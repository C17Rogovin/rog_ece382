#include "pong.h"

/*
 * pong.c
 * Author: Max Rogovin
 * Date: 8 Oct 15
 * Description: header file for a pong game setup
 */

ball createBall(int startX, int startY, int XballVelocity, int YballVelocity, unsigned char radius){

	ball inputBall;
	inputBall.radius = radius;
	inputBall.position.x = startX;
	inputBall.position.y = startY;
	inputBall.velocity.x = XballVelocity;
	inputBall.velocity.y = YballVelocity;

	return inputBall;
}


ball moveBall(ball movingBall){

	movingBall.position.x = movingBall.position.x + movingBall.velocity.x;
	if (movingBall.position.x <= 0 || movingBall.position.x >= SCREEN_X){
		movingBall.velocity.x = movingBall.velocity.x * NEG_ONE;
	}
	movingBall.position.y = movingBall.position.y + movingBall.velocity.y;
	if (movingBall.position.y <= 0 || movingBall.position.y >= SCREEN_Y){
		movingBall.velocity.y = movingBall.velocity.y * NEG_ONE;
	}
	return movingBall;
}

