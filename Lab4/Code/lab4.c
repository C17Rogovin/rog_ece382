#include <msp430g2553.h>
#include "pong.h"

extern void initMSP();
extern void initLCD();
extern void clearScreen();
extern void drawBox(unsigned int col, unsigned int row, unsigned char color);
extern void Delay160ms();
extern void Delay40ms();


#define		TRUE			1
#define		FALSE			0
#define		UP_BUTTON		(P2IN & BIT2)
#define		DOWN_BUTTON		(P2IN & BIT1)
#define		LEFT_BUTTON		(P2IN & BIT0)
#define		RIGHT_BUTTON	(P2IN & BIT3)
#define     COLOR_BUTTON    (P1IN & BIT3)
#define     BLACK 0x0000


void req_functionality(){
	unsigned int x, y, button_press;

	button_press = FALSE;

	unsigned short color;
	unsigned short colorArray[] = {0x4416, 0x07EF, 0xFB56, 0xFFFF, 0xFFE0};
	int colorIndex = 0;

	x=0;
	y=0;
	color = colorArray[colorIndex];

	drawBox(x, y, color);
	Delay160ms();

	while(1) {

			if (UP_BUTTON == 0){
				Delay160ms();
				//drawBox(x, y, BLACK);
				y = y - 10;
				drawBox(x, y, color);
			}
			if (DOWN_BUTTON == 0){
				Delay160ms();
				//drawBox(x, y, BLACK);
				y = y + 10;
				drawBox(x,y,color);
			}
			if (LEFT_BUTTON == 0){
				Delay160ms();
				//drawBox(x, y, BLACK);
				x = x - 10;
				drawBox(x,y,color);
			}
			if (RIGHT_BUTTON == 0){
				Delay160ms();
				//drawBox(x, y, BLACK);
				x = x + 10;
				drawBox(x,y,color);
			}
			if (COLOR_BUTTON == 0){
				Delay160ms();
				//drawBox(x, y, BLACK);
				colorIndex += 1;
				if (colorIndex > 4){
					colorIndex = 0;
				}
				color = colorArray[colorIndex];
				drawBox(x,y,color);
			}
			if (y >= 310){
				y = 0;
			}
			if (x >= 230){
				x = 0;
			}
		}
}

void b_functionality(){
	unsigned int ball_x, ball_y, button_press;
	int velocity_x, velocity_y;

	button_press = FALSE;

	unsigned short color;
	unsigned short colorArray[] = {0x4416, 0x07EF, 0xFB56, 0xFFFF, 0xFFE0};
	int colorIndex = 0;

	velocity_x = 2;
	velocity_y = 2;
	ball_x = 0;
	ball_y = 280;
	color = colorArray[colorIndex];

	ball alphaBall = createBall(ball_x,ball_y,velocity_x, velocity_y, 0xA);

	while(1){
			drawBox(alphaBall.position.x,alphaBall.position.y,BLACK);
			//Delay40ms();
			alphaBall = moveBall(alphaBall);
			drawBox(alphaBall.position.x,alphaBall.position.y,color);
			Delay40ms();
		}


}

void a_functionality(){
	unsigned int ball_x, ball_y, button_press, paddle_x, paddle_y;
	int velocity_x, velocity_y;

	button_press = FALSE;

	unsigned char winState;
	winState = TRUE;

	unsigned short color;
	unsigned short colorArray[] = {0x4416, 0x07EF, 0xFB56, 0xFFFF, 0xFFE0};
	int colorIndex = 0;

	velocity_x = 2;
	velocity_y = 2;
	ball_x = 100;
	ball_y = 180;
	color = colorArray[colorIndex];

	paddle_x = 150;
	paddle_y = 310;

	ball alphaBall = createBall(ball_x,ball_y,velocity_x, velocity_y, 0xA);

	while(winState){
			if (LEFT_BUTTON == 0){
				Delay160ms();
				drawPaddle(paddle_x, paddle_y, BLACK);
				paddle_x -=10;
			}
			if (RIGHT_BUTTON == 0){
				Delay160ms();
				drawPaddle(paddle_x, paddle_y, BLACK);
				paddle_x += 10;
			}
			if (paddle_x < 0){
				paddle_x = 0;
			}
			if (paddle_x > 290){
				paddle_x = 290;
			}

			drawBox(alphaBall.position.x,alphaBall.position.y,BLACK);
			if (alphaBall.position.y < 300){
				alphaBall = moveBall(alphaBall);
			}
			//alphaBall= moveBall(alphaBall);
			if (alphaBall.position.y >= 300){
				if ( (paddle_x < (alphaBall.position.x - 10)) && (paddle_x > (alphaBall.position.x - 50)) ){
					alphaBall.velocity.y = alphaBall.velocity.y * -1;
					alphaBall = moveBall(alphaBall);
				}
				else{
					winState = FALSE;
				}
			}
			drawPaddle(paddle_x, paddle_y, color);
			drawBox(alphaBall.position.x,alphaBall.position.y,color);
			Delay40ms();
		}
		clearScreen();

		unsigned int text_coord_x, text_coord_y;
		text_coord_x = 0;
		text_coord_y = 0;

		drawBox(text_coord_x,text_coord_y,color);  // Y
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 20;
		text_coord_x -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);


		text_coord_x += 30;                         //o
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x
		-= 10;
		drawBox(text_coord_x,text_coord_y,color);

		text_coord_x += 40;                       //u
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 20;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x -= 10;
		drawBox(text_coord_x,text_coord_y,color);

		text_coord_y += 40;                        //new line
		text_coord_x = 20;
		drawBox(text_coord_x,text_coord_y,color);

		text_coord_x -= 10;                       //capital S
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x -= 10;
		drawBox(text_coord_x,text_coord_y,color);

		text_coord_x += 40;                       //u
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 20;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x -= 10;
		drawBox(text_coord_x,text_coord_y,color);

		text_coord_x += 30;
		text_coord_y -= 20;                        //c
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 20;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);

		text_coord_x += 40;                        //k
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 10;
		text_coord_y += 20;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_x += 10;
		text_coord_y += 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 20;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y -= 10;
		drawBox(text_coord_x,text_coord_y,color);
		text_coord_y += 40;
		drawBox(text_coord_x,text_coord_y,color);


}

void drawPaddle(int x, int y, unsigned short color){
	drawBox(x,y,color);
	drawBox(x+10, y, color);
	drawBox(x+20, y, color);
	drawBox(x+30, y, color);
}

void main_lab(){



	// === Initialize system ================================================
	IFG1=0; /* clear interrupt flag1 */
	WDTCTL=WDTPW+WDTHOLD; /* stop WD */


	initMSP();
	Delay160ms();
	initLCD();
	Delay160ms();
	clearScreen();
	Delay160ms();

	//req_functionality();
	//b_functionality();
	a_functionality();


}
