## Lab 4: A New Language ##

### Objectives ###

The Objectives of the lab were to:

* For required functionality: Create an etch-a-sketch style program with color toggle, and button input to move the cursor.  
* For B Functionality: Create a bouncing block that bounces off the edge of the screens, staying within the screen at all times
* For A Functionality: Create Pong


### Purpose ###

The purpose of the lab was to learn how to use C to code on an MSP430 and use C to call assembly code to program the MSP430.  Additional purposes include using structs and other advanced C coding techniques.  

## Prelab ##

The prelab activities primarily involved looking at the storage and manipulation of data on the MSP430.  The tables filled out included this data table on data types:

![DataType](images/datatype.png)

The following table shows how one might declare a struct of certain data types:

![struct](images/struct.png)

The final half of the prelab was to take a provided program and use the coding suite to examine algorithms and the assembled code.  Presumably, this was to practice for the actual lab programming.  

Results of algorithm:

![algorithm](images/algorithm.png)

Assembled code:

![disassembly](images/disassembly.png)

Important locations of stuff in assembled code

![locations](images/locations.png)

There were also two questions about using assembly code in C, questions and answers shown below:

![qanda](images/qanda.png)

## Required Functionality: Etch-a-sketch ##

The prompt for required functionality is as shown: 

> Modify your assembly drawBox function to take in 3 values: an x coordinate, a y coordinate, and a color.

> Some hints have been included in the lab4.c file to get you started. Create an etch-a-sketch program using the directional buttons of the LCD boosterpack to control the position of the paint brush. The paint brush will draw 10x10 blocks of pixels. The user will change the position of the paint brush by pressing the directional buttons. Each button press will move the cursor 10 pixels in the direction pressed (see table below). Pressing the auxiliary button (S1) will toggle the mode of the paint brush between filling squares and clearing squares.

The most important thing is to properly import the functions from assembly into C.  The .global [functionname] was put in for the drawBox and Delay160ms functions (in the assembly), and declare the extern void [functionname] in the C file.  Extern declarations shown below:

![.global](images/global.png)
![extern](images/extern.png)

drawBox had to be modified to take a color input.  Because it already took in start coordinates in the r12 and r13 registers, the function could use call the parameters drawBox(start_x_coordinate, start_y_coordinate).  Because of how C calls assembly functions, by adding another input parameter, say, color, the input would be stored in the r14 register, because a function call like function(a,b,c,etc...) stores a in r12, b in r13, etc.  

Beyond that, the functionality was just a matter of changing coordinates of the starting x and y coordinates after a corresponding button press, then drawing another square.  Shown below:

![coords](images/coordchange.png)

For the button to change color, i used an array and indexed through the array to change colors.   First I found the color I was on, and then indexed one more into the array.  If the array went over the length of the array, I reset the index to 0.  I also had the button press draw the new colored square to have a nice easy way to tell what color was selected. Code shown:

![color](images/colorarray.png)
![color](images/color.png)

Then I trapped the program in a while loop to keep the program running.  For efficiency, I had the box drawn at the end of each while loop.  

## B Functionality: Bouncing Ball ##

The bouncing ball was more difficult.  This is mainly because the use of structs was required to get relatively simple code.  The simple solution was to use the code I made from assignment 7 "pong" and utilize the header files to make a ball bounce.  By drawing a square and setting the radius of the ball object (it wasn't really a radius, but a square) to the same size, and using the property coordinates from the struct to draw the square, I can manipulate the ball on the screen using concise code.  

The header file prototypes are as shown:

![prototypes](images/prototypes.png)

The code for moving and bouncing the object is as shown:

![bounce](images/bounce.png)

The best part about doing all the code work in the header file is that all I have to do is just constantly move the ball.  The header file already handles the bouncing off the wall.  No need for any inputs, just start the ball, give it a velocity in the x and y directions and let it fly.  I opted to use a 40ms delay after drawing each box to prevent the program from trying to draw a new box before it drew the previous box.  Also, before I draw the next box, i re-draw the old box in black to "erase" it.  This makes the ball/square appear as if it moving.  Short, simple, and to the point.

![loop](images/loop.png)

## A Functionality: Pong ##

Pong was a tad harder still.  In this situation, the code for pong had to handle the ball, paddle, and when to bounce or end the game.  The first step was to pick an approach.

The ball was handled exactly the same as in the B functionality code, because it used the same header files and initialization.  The first step was to pick when to end the game.  I opted to end it when the ball got below the paddle, i.e. when the c-coordinate of the lower edge of the ball was below the upper edge of the paddle.  In this case, this meant the exit condition was when the ball position was 300 or greater on the y-axis.  The screen ends, but I have to account for the position of the ball and make a case exception if the paddle allows the ball to bounce.  I opted to create a variable to hold a true/false variable called winState to make my code easier to read.  If the position of the paddle covers the ball, the ball will bounce, but if it doesn't, the winState switches, and the program will quit at the top of the while loop and go to the lose game screen.

![endgame](images/endgame.png)

Drawing the paddle was a tad harder.  I just drew multiple squares using indexed locations along the x axis to make the paddle 4-5 squares wide.  This way, I can just change the paddle location, check for screen run-off and then set the coordinate to make sure the paddle isn't half-off the screen.  Theoretically, I could have initialized part of the paddle as a ball object to take advantage of some of the bouncing object.  Heck, if i wanted to I could have modified pong to have a paddle that works based off of vector velocity rather than position and take advantage of the bouncing code.  I made a drawPaddle function to handle this.  

![drawpaddle](images/drawpaddle.png)

I also take advantage of efficiency by only drawing the ball and paddle once per cycle, AFTER calculating all coordinate changes, but before the loop checks the endgame condition.  

![drawonce](images/drawonce.png)

## Bonus Functionality ##

I programmed my board to draw a message to the user if they lose the game of pong.  I used the drawsquare function to draw multiple boxes to spell out the message after the while loop breaks in the pong game.  It's not terribly efficient, and it takes up a few hundred bytes in memory, but it's certainly memorable.  

Below is the code demonstrating how I drew the first letter:

![letter](images/letter.png)

## Debugging ##

The biggest problem with the code I used and some of the stuff provided was that it used some chars and unsigned chars rather than ints.  This limitation showed issues in the y-axis, because the screen's 320 pixels is greater than an 8-bit max value of 255.  So for everything except color (which i kept as unsigned char in all declarations) i switched to either ints (for stuff like velocities which could be negative) or unsigned ints in the case of coordinates, to keep values able to handle numbers > 255.  It worked like a charm, and fixed a vexing error where the row would truncate at 256 back to 0 and make the game handle all funky with the paddle and ball in the lower half of the screen in all functionality levels.

## Observations and Conclusions ##

In the end, data handling is very important and subtle.  Small errors in declarations could lead to vexxing problems.  C is much more readable and easy to work with than assembly.  The ability to call assembly for more low-level functions was more efficient and a good use of previously existing code.  All in all, this was easier to code than previous labs because of the use of C.  

## Documentation ##

* EI W/ Capt Falkinburg: Troubleshooting and my general stupidity
* C2C Christian Arnold: 
* C2C Daniel Villareal: Regarding some help with the assembly