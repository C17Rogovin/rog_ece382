/*
 * moving_average.c
 *
 *  Created on: Oct 13, 2015
 *      Author: C17Max.Rogovin
 */

#define N_AVG_SAMPLES 2

// Moving average functions
int getAverage(int array[], unsigned int arrayLength){

	unsigned int init_counter = 0;
	int movingArray[arrayLength];
	int* array_p = array;

	while(init_counter < (N_AVG_SAMPLES-1)){  //init loop, indexing for 0
		movingArray[init_counter] = 0;
		init_counter += 1;
	}

	int resultArray[arrayLength];              //set up result array
	int* resArray_p = resultArray;  //pointer
	int indexing = 0;               //indexing for how many places over in the input array I am

	while((indexing + N_AVG_SAMPLES)< arrayLength){
		//get average of N_AVG_SAMPLES
		int place_counter = 0;
		int running_total = 0;
		while(place_counter < N_AVG_SAMPLES){
			running_total = *array_p + indexing + place_counter;
			place_counter += 1;  //we moved a place along
		}
		running_total = running_total / N_AVG_SAMPLES;  //this is the average

		//store in result array

		*resArray_p = running_total;  //store the average

		//index
		indexing += 1;    //index along the input array
		resArray_p += 1;  //move to next spot in result array
	}
	return resultArray[];
}

// Array functions
int max(int array[], unsigned int arrayLength){

	int maxval = 0;
	unsigned int array_count = 0;         //how many places along in the array we are
	int* array_p = array;

	while(array_count <= arrayLength){
		if(*array_p > maxval){
			maxval = *array_p;
		}
		array_p += 1;
		array_count += 1;
	}
	return maxval;
}

int min(int array[], unsigned int arrayLength){

	int minval = 256;
	unsigned int array_count = 0;         //how many places along in the array we are
	int* array_p = array;

	while(array_count < arrayLength){
		if(*array_p < minval){
			minval = *array_p;
		}
		array_p += 1;
		array_count += 1;
	}
	return minval;
}

unsigned int range(int array[], unsigned int arrayLength){

	int minval = 0;
	int maxval = 0;
	int range = 0;

	minval = min(array,arrayLength);
	maxval = max(array,arrayLength);
	range = maxval - minval;

	return range;
}

