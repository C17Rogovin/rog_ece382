#include <msp430.h> 
#include "moving_average.h"

/*
 * main.c
 */
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    int array1[] = {45, 42, 41, 40, 43, 45, 46, 47, 49, 45};
    int average = 0;

    average = getAverage(array1, 10);

    /* TEST CODE FOR ARRAY FUNCTIONS
    int max_num = 0;
    int min_num = 0;
    int range_num = 0;

    max_num = max(array1,10);
    min_num = min(array1,10);
    range_num = range(array1,10);

	return 0;
	*/
}
