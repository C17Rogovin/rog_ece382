#include <msp430.h> 

/*
 * main.c
 */
float tempC;
float tempF;


void main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	
    ADC10CTL0 = ADC10SHT_3|ADC10ON|REFON|SREF_1; // ADC10ON, interrupt enabled

    ADC10CTL1 = INCH_4|ADC10SSEL_3|ADC10DIV_7;// input A4
    ADC10AE0 |= BIT4;                         // PA.1 ADC option select

    P1DIR |= BIT0|BIT6;

    tempC = 0;
    tempF = 0;

    while(1){
    	ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start

    	__delay_cycles(50000);
    	tempF = ADC10MEM;
    	tempF = (tempF * 1500.0/1024.0 )/10.0 ;
    	tempC = (5.0 / 9.0) * (tempF - 32.0);

    	if ((tempC > 0) && (tempC < 30)){

    	   P1OUT |= BIT0;                       // LED on
    	   __delay_cycles(1000000);
    	}
    	else if (tempC > 30){
    	   P1OUT |= BIT6;                       // LED on
    	   __delay_cycles(1000000);
    	}
    	else{
           P1OUT &= ~BIT0;
		   P1OUT &= ~BIT6;                      // LEDs off
		   __delay_cycles(1000000);
    	}
   }
}



