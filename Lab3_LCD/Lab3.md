## Lab 3: Revenge of the SPI ##

### Objectives ###

The Objective of the Lab was to be able to produce the waveforms showing the function of the setArea subroutine, as well as draw a square using said function.  Bonus objectives were to make the square move in a given cardinal direction using extra input buttons.  Bonus objective was to write a 5-letter name on the LCD screen.

### Purpose ###

The purpose of the lab was to learn how to use Serial Interfaces, and use input and output on a microcontroller device.  This involved multiple reference documents and testing methods.  

## Prelab ##

The prelab involved filling out several tabled.  Most of them involved identifying the hardware components of input/output (IO) operations and properly correlating them to software ports and physical pins on the MSP430 microcontroller.

In this case, it involved correlating the ports (P1/2.#) to a physical pins, and the functions each pin was performing in the program provided/modified.  

## Delay Function ##

The first actual part of coding was building a delay function of sufficient cycles to hold the CPU captive for 160ms to prevent the button bouncing from interfering with desired function.  The button is a mechanical device, and the spring can bounce and cause multiple inputs.  This can be avoided by delaying for 160ms.  Additionally, the LCD screen takes time to set up, and you cannot send commands or data to the LCD screen while it is initializing, or errors occur.  

At 8MHz, about 1.28 million cycles is needed to occupy .16ms.  I built a loop with a 7 cycle setup and 7 cycle closeout, and a 28 cycle loop (repeated 0xB292 times) to get ~1.28 million cycles.  The code is shown below.

![Delay](images/delay.png)

## Logic Analyzer Waveforms ##

With the delay function properly coded, the next step was to evaluate the setArea function.  The subroutine functions by setting the drawing area.  In the subroutine, there are 11 times that data or commands are sent to the LCD screen and the serial interface becomes active.  To track the data, the time intervals, Serial Clock (SCLK), MOSI signal, Chip Select (CS) signal, and Data Command (DC) select.  

Shown below are the waveforms, associated lines, data transmitted, and purpose of the data/command transmitted.  

Line 394 was a write command, setting the column address at 0x2A.

![394](images/line394.png)

Line 397 was a write data instruction, writing the XStartMSB value of 0x00.  

![397](images/line397.png)

Line 400 was a write data instruction, writing the xStart (other half) value of 0x9B.

![400](images/line400.png)

Line 407 was a write data instruction, writing the xEnd (other half) value of 0xA4. 

![407](images/line407.png)

Line 410 was a write command, setting the row address at a value of 0x57.

![410](images/line410.png)

Line 414 was a write data instruction, writing the yStartMSB value of 0x00.

![414](images/line414.png)

Line 417 was a write data instruction, writing yStart (other half) value of 0x2F.

![417](images/line417.png)

Line 422 was a write data instruction, writing the yEndMSB value of 0x00.  

![422](images/line422.png)

Line 425 was a write data instruction, writing the yEnd (other half) value of 0x2F.  

![425](images/line425.png)

Line 428 was a write command, setting the RAM memory write to the command value of 0x2C (referenced from section 8.2.22 of the ILI9340 tech doc).  

![428](images/line428.png)

Taking this information into account, and referencing the ILI9340 tech doc, several things can be understood from the waveforms.  First, the DC signal is high when data is being transmitted, and low when a command is being transmitted (7.1.3, 7.1.6).

Referencing section 7.1.9, the write cycle sequence involves starting the SCLK when CS has a falling edge.  This starts data transmission.   In order to write coordinates and pixels to the LCD screen, the column, followed by the X coordinates must be set.  Then the row and Y coordinates.  The column/row set is a command.  The rest is coordinate data.  The final RAM command transfers the data from the MCU to frame memory.  Once this command is successful, it resets the column and page register (i.e. column and row).  

## B Functionality + Set Area ##

B functionality was using the setArea function to draw a 10x10 pixel square.  The simple approach was to define the area of the square using the 4 specified registers.  Each was set up at the start coordinate for the first point (0,0), the value copied into the end x and y coordinate, and 9 added.  Because the MSP430 indexes from zero, this defines an area of 10x10 pixels.  When the values are run through the setArea functions, it sets up the area to be drawn.  When run through a modified, looped drawLine (renamed drawSquare), it draws the 10 lines of pixels.

Code for coordinate setting: 

![Coordinates](images/setcoords.png)

Code for looping to draw lines: 

![lines](images/looplines.png)

The code defaults by drawing a square at the starting location.  The register r4 is used to track the number of lines (hard set at 10) to be drawn on the y-axis.  The area set to draw has to be set, but the program actually has to be told to draw the lines and write the data to the LCD. Like all other subroutines, this one pushes values at the start, and pops them back into the registers at the end.

## A Functionality: Making it Move ##

This was a little tougher.  Four extra buttons had to be set up and mapped to the correct ports.  I also changed the S1 button to function as a reset.  The buttons were then polled to, when pressed, jump to the right section of code to cause the coordinates to change, screen to be erased, and a new, coordinate-shifted square to be drawn.  

Button setup:

![Buttons](images/buttonsexposed.png)

Polling loop:

![Polling](images/pollingbuttons.png)

Coordinate-shifting and square drawing:

![shifting1](images/shifting1.png)
![shifting2](images/shifting2.png)

## Debugging ##

Details, details, details.  I believe I had the code properly set at least twice before I finally got it functioning.  The biggest problem was that I had not set the coordinates correctly each time a shifting subroutine was called.  I only set the changed x or y coordinate, and did not adjust the other axis' coordinates to properly draw a square.  Once this was fixed, everything fell into place.

Another problem I ran into was the "fluff" included in the original drawLine subroutine.  I removed most of that and adjusted the "register juggling" to take into account how the fluff had worked.  This simplified my job, and helped out a lot. 

## Observations and Conclusions ##

Tech docs are important references for complex devices.  Learning to use these could greatly simplify any task to be coded on any device.  They greatly aide in an understanding of the code needed, and how to accomplish a coding task.  

## Documentation ##

* EI W/ Capt Falkinburg: Troubleshooting and my general stupidity
* C2C Christian Arnold: Discussed approaches and waveform setup and helped me get started on approaching the problems involved