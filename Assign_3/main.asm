;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section
            .retainrefs                     ; Additionally retain any sections
                                            ; that have references to current
                                            ; section
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

;-------------------------------------------------------------------------------
           mov #0x1001, &0x0216 ; set initial value to compute        INPUT HERE
           mov #0x1234, r4      ; set comparison value

           mov &0x216, r5       ; set val to be manipulated
           sub r4, r5           ; compare values (i.e. subtract)
           jn if_neg            ; if its negative, jump to if_neg
           jz if_neg            ; also jump if equal

		   mov #0x0, r5        ; reset modified value to zero
		   mov #20, r6         ; set 20 dec value
positive   add r6, r5          ; start adding 20! to r5
           sub #1, r6          ; subtract 1 from the 20! value
           mov r5, &0x206
           jz forever            ; if r6==0, end of program
           jmp positive          ; go back and loop

if_neg     mov #0x1000, r5        ; set 0x1000
           mov &0x216, r6         ; set val to be manipulated
           sub r5, r6             ; compare to 1000

           jn less_thou             ; if its negative, jump to less_thou (less than hex thousand)
           jz less_thou             ; also jump if equal

		   mov #0, r7               ; must be set before operation
           mov &0x216, r5
           add #0xEEC0, r5

           addc #0,r7      ; HELP NEEDED HERE

           mov.b r7, &0x202  ; TO HERE
           jmp forever

even       mov &0x216, r5
           rra r5
           mov r5, &0x212
           jmp forever


less_thou  mov &0x216, r5       ; set val to be manipulated
           and #0x0001, r5      ; checking for even and odd

           jz even              ; jump if its even (i.e. last digit is even)
                                ; HELP NEEDED HERE TOO
           mov &0x216, r5
           rla r5
           mov r5, &0x212




forever jmp forever
;-------------------------------------------------------------------------------


;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect 	.stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
