;-------------------------------------------------------------------------------
;           Coded by: C2C Max B Rogovin, CS-36
;           Program: Lab 1, calculator
;           Due 3 Sept 2015
;           Documentation: EI with Capt's Falkinburg, V. Trimble, assitance from C2C Griffith on loops/jumps and program structure
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
;-------------------------------------------------------------------------------
             .text                           ; Assemble into program memory

INSTRUCTION: .byte 0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55

ADD_OP:     .equ 0x11
SUB_OP:     .equ 0x22
MULT_OP:    .equ 0x33
CLR_OP:     .equ 0x44
END_OP:     .equ 0x55
max_num:    .equ 0xFF
min_num:    .equ 0x00

one:        .equ 0x0001
zero:       .equ 0x0000

             .data                          ;
MyResults:   .space 20                      ;

             .retain                        ; Override ELF conditional linking
                                            ; and retain current section
             .retainrefs                    ; Additionally retain any sections
                                            ; that have references to current
                                            ; section
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer
;-------------------------------------------------------------------------------
               mov #MyResults, r4        ; store results loc in mem
               mov #INSTRUCTION, r5      ; store instructions in a register
               mov #zero, r11               ; needs to be set for multiplication algorithm
               mov #zero, r12               ; needs to be set for multiplication algorithm
               mov #zero, r15               ; needs to be set for multiplication algorithm


FIRST_OPERAND  mov.b @r5+, r6            ; get first operator

OPERATOR       mov.b @r5+, r7            ; get operand

SECON_OPERAND  mov.b @r5+, r8            ; get second operator

               cmp.b #ADD_OP, r7         ; check for add operator
               jeq ADD_OPERATION         ; jump to ADD_OPERATION if operator is equal to proper value

               cmp.b #SUB_OP, r7         ; check for add operator
               jeq SUB_OPERATION         ; jump to SUB_OPERATION if operator is equal to proper value

               cmp.b #MULT_OP, r7        ; check for add operator
               jeq MULT_OPERATION        ; jump to MULT_OPERATION if operator is equal to proper value

               cmp.b #CLR_OP, r7         ; check for add operator
               jeq CLR_OPERATION         ; jump to CLR_OPERATION if operator is equal to proper value

               cmp.b #END_OP, r7         ; check for add operator
               jeq forever               ; jump to ADD_OPERATION if operator is equal to proper value

               jmp error                 ; if no operator found, trap CPU in a way that designates this as an error

ADD_OPERATION  add.b r6, r8
               jhs MAX_VAL

RETURN_TO_ADD  mov.b r8, 0(r4)
               mov.b r8, r6              ; put the number into the first operand
               inc r4
                                         ; put number into memory
               jmp OPERATOR              ; move to next operation

SUB_OPERATION  sub.b r8, r6
               jn MIN_VAL

RETURN_TO_SUB  mov.b r6, 0(r4)
               inc r4
                                         ; put number into memory
               jmp OPERATOR              ; move to next operation

MULT_OPERATION cmp #one, r8              ; check if 2nd number = 1
               jeq  END_MULT             ; if it is, maxcheck and store in memory

               cmp #zero, r6             ; if first operator zero, output is zero
               jeq MULT_ZERO

               cmp #zero, r8             ; if second operator zero, output is zero
               jeq MULT_ZERO

               bit #one, r8
               jz DELETE_ROW             ; if divided number is even, track result of left row to deduct from final

MULT_RETURN    add r6, r11               ; gathers sum of multiplied numbers           THIS WASNT IN THE LOOP, biggest problem
               rla r6                    ; multiply by two  DO NOT CARRY
               rra r8                    ; divide by two    DO NOT CARRY

               bit #one, r8
               jz DELETE_ROW             ; if divided number is even, track result of left row to deduct from final

               jmp MULT_OPERATION        ; iterate

END_MULT       add r6, r11
               sub r12, r11              ; subtract even row multipled values from total value (reference the algorithm)
               cmp #max_num, r11         ; needed to trigger the max value
               jhs MAX_MULT_VAL
               mov.b r11, 0(r4)

               inc r4

               jmp OPERATOR              ; move to next operation

MAX_MULT_VAL   mov.b #max_num, r6
               mov.b r6, 0(r4)           ; put number into memory
               inc r4

               jmp OPERATOR

MULT_ZERO      mov.b #zero, r6           ; in the rare case an operand is zero, just store zero in mem, otherwise keeps looping
               mov.b r6, 0(r4)           ; put number into memory
               inc r4

               jmp OPERATOR

DELETE_ROW     add r6, r12
               jmp MULT_RETURN

MAX_VAL        mov.b #max_num, r8
               jmp RETURN_TO_ADD

MIN_VAL        mov.b #min_num, r6
               jmp RETURN_TO_SUB

CLR_OPERATION  mov.b r8, r6
               mov.b #0x00, r8           ; move into r8, perhaps use r8 for the zero check?

               mov.b r8, 0(r4)           ; put number into memory
               inc r4

               jmp OPERATOR              ; move to next operation

END_OPERATION  jmp forever

error   jmp error                        ; if CPU trapped here, no op code present/found

forever jmp forever
;-------------------------------------------------------------------------------


;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect 	.stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
