# Lab 1: The Multiplication Menace #

## Purpose and Objectives ##

The purpose of the lab was to learn how to properly code in assembly, and use the microcontroller.  The objective was to make the calculator that could add, subtract and clear.  Additional objectives included multiplication and size checking.

## Preliminary design ##

### How to Calculator ###

A calculator does basic math operations.  In this case, the calculator reads a set of instructions and performs the operations.  In this particular instance, the calculator is an unsigned 8-bit, 4-operation calculator.  It adds, subtracts, multiplies, and clears.  This calculator will store the results sequentially in RAM, and then endlessly loop the CPU without altering anything to display the results (and not crash violently or reset).

### Prelab ###

The purpose of the prelab was to prepare a flowchart of how the calculator will function. Shown below is that flowchart.

![Memory Dump](images/prelab.PNG)

The program starts by setting the memory addresses of the instructions in ROM into RAM.  Then, the first operand, operator, and second operand are moved into registers.  The operator is then compared with the known operators to check if an operator is present, and which one.  The program then executed the operation associated with the operand (using the specified operands), writes the result into RAM, and loops.  

In the case of the number being too large or too small (B functionality), the program will compare values and then correct the size error.  If the number exceeds limits, it is set at the minimum or maximum value.

In the case of an END_OP, the program will simply trap the CPU.   

### B Functionality ###

In addition to the core functionality, B functionality level meant coding the calculator to handle overflows.  In the case of a positive overflow of an 8-bit number, anything exceeding 0xFF would be set to 0xFF and written to memory as such, while a negative overflow (i.e. numbers less than 0x00) would be set to 0x00 and written to memory as such.  Because the calculator is unsigned 8-bit, the positive check was only needed on the multiplication and addition functions, and the negative check on the subtraction function.  This slimmed the code down somewhat.  

Both functions worked by checking the flags for overflows.  Using the JHS for large number overflows and JN for negative number overflows, it was possible to jump to the lines of code needed to set the proper value.

Addition shown below:

![Memory Dump](images/addoperation.PNG)

Subtraction shown below:

![Memory Dump](images/suboperation.PNG)

Multiplication shown below:

![Memory Dump](images/multoperation.PNG)

### A Functionality ###

A functionality requires multiplication.  Because there is no multiplication circuit in the ALU or microcontroller, in order to implement multiplication algorithms must be used.  While simple approaches like looped addition would work, they tend to be slow and inefficient, completing in O(n) time.  The goal of A functionality is to implement multiplication in Olog(n) time.  I did some research on the subject, and settled on an algorithm called "Russian Peasant Multiplication."  More on this later.  

## Algorithms ##

### Op Codes ###

In order to check for op codes, several things must be set up and taken into account.  First, the Op Code and instruction values were stored in ROM for later comparisons, shown below:

![Memory Dump](images/ROM.PNG)

Then, the operands and operator from the instructions were loaded into memory.

![Memory Dump](images/RAMload.PNG)

Finally, the op code is compared to the known values.  If they match, the program jumps to the appropriate block of code and continues the execute.  Comparisons shown below:

![Memory Dump](images/opcodecompare.PNG)

Additionally, if the op code is not found, the program will automatically jump to trap the CPU to avoid an error or bad operations/calculations.  I chose to trap it in such a way that the error would be obvious.  Unconditional jump and trap-loop shown.

### Russian Peasant Multiplication ###

To multiply, I used an algorithm known as Russian Peasant Multiplication.  This method works by using doubling and halving.  This is advantageous because of the nature of binary, multiplying and dividing by two is simply a left and right shift of bits, respectively.  

Use of the algorithm is explained [here](http://mathforum.org/dr.math/faq/faq.peasant.html).

[This article](http://mathforum.org/dr.math/faq/faq.peasant.html) alludes to the binary underpinnings of the algorithm, but does not quite get the explanation right.  

The real workings of the algorithm are rooted in bit-shifting.  Basically, any time the halving operand is even, the last bit is zero.  But for every time a number is halved, the other operator is doubled.  By tallying the total of the doubled numbers every time the halved operand is odd, you are essentially bit shifting each number, multiplying the first operator by a power of two that is marked with a '1' bit in the halving operand.  

The benefit of the Russian Peasant algorithm is that it uses arithmetic shifts and drops the carry in all cases.  Functionally, this is achieved using the rla and rla commands.  

## Well-formatted code ##

### Adding ###

Shown above.

### Subtracting ###

Shown above.

### Multiplying ###

Shown in following sections.  

### Reading from Memory ###

The program is designed to loop back to the operator load into register operation in every case but CLR_OP.  In the CLR_OP, it loops back into the first operand load operation.  This is how the program reads from memory.  It's also a code-efficient operation.  

### Writing to Memory ###

Writing to memory typically looks like this:

![Memory Dump](images/writetomem.PNG)

The second register is written to the address stored in r4, which is the next available location in RAM.  The second operand, the result of the operation, is then moved to the first operand position.  Then the program jumps to load the next operator and operand into the registers, and perform the next operation.  This code is repeated often throughout the program.  

## Debugging ##

### Multiplication ###

The multiplication algorithm was tricky to debug.  There were several checks to perform in order to set up the multiplication, and the looping and conditionals were complicated for assembly.  The biggest problem was handling the numbers in the 8-bit size, and how to loop given certain conditions.   

#### 8-bit and 16-bit, a discussion on overflows ####

Due to the nature of the bit shifting, it is possible for a running tally of the doubled operand to exceed the 0xFF limit but have the final result be below that limit.  In order to avoid a situation where the result is unclear, I used a full word (16 bits) to store the running tallies.  While this does not conform to an 8-bit calculator, there was no other way to accurately multiply.  

Because my algorithm keeps a total of both the total of the multiplied operand and the "reject" rows, and then subtracts the reject total from the multiplied operand total, the number could theoretically exceed 8 bits if the length halving operand plus the length of the doubling operand is greater than eight bits, and possibly as high as 16 bits.  Then, if the dividing operand has enough 0 bits, it could bring the running total down under 8 bits.  Thus the easiest way to get around this limitation is to use a full word in the registers to keep track of the running numbers and tallies.  

In the end, it is easy to just compare the value of the result to 0xFF, and if it is greater than 0xFF set it as 0xFF.  In this sense, it is very similar to the addition maximum value set program.  

#### Looping, Multiplication ####

The ordering in the multiplication algorithm was very important.  However, the easiest way of dealing with it was to explain the algorithm plain english, and then translate that into code.  

In a few steps, the algorithm was thus:

1. Check the halving operand to see if it is equal to 1.
   * if it is, subtract the rejected values from the running total
   * check the number to make sure it doesnt exceed the maximum value (set if it exceeds)
   * store the number in RAM
   * proceed to the next operation
2. If the doubling or halving operator is zero, set and store an output of 0x00.
3. Check the halving operand to see if it is odd.  If it isnt, add the doubling operand to the running tally of "reject" numbers.  
4. Picking up from MULT_RETURN, add the doubling operand to the running tally and perform the arithmetic shifts (left on the doubling operand, right on the halving operand).
5. Halving operand checked again.
6. Iterate by looping to the beginning of the multiplication algorithm and performing the checks again.

The checks needed to be performed before the shifts to make sure the halving operand doesn't run past one and loop endlessly.  The tallies are also placed so that the shifts don't affect the tallies, and only tally after the comparison or operation where it should be.  This way you don't double tally or miss a tally while looping, or more difficult, not looping (i.e. on even halving operands and the halving operand being one).  

### Loops in addition/subtraction ###

Addition and subtraction were not looping functions.  They used a compare and different conditional jumps to check the values versus the maximum and minimum value.  If the condition is triggered by a value exceeding limits, the number is set in the register, and returns to the addition/subtraction function and then writes the value into memory.  

Shown below is the code for the minimum and maximum value jumps.

### Test cases, edge cases ###

## Testing methodology / results ##

To test the functionality of my program, I used a number of various test cases.  For the required and B functionality, I used the test cases provided.  For multiplication I had to make a variety of test cases for different edge cases.  This was to test the program without wading through a lot of other functions in the provided A functionality test case.  

### Test cases, required functionality ###

The test case provided was: 0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55

The expected result was:  0x22, 0x33, 0x00, 0x00, 0xCC

The picture below shows the functionality working properly:

![Memory Dump](images/reqfunctionality.PNG)

### Test cases, B functionality ###

The test case provided was: 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55

The expected result was: 0x22, 0x33, 0x44, 0xFF, 0x00, 0x00, 0x00, 0x02

The picture below shows B functionality working properly:

![Memory Dump](images/bfunctionality.PNG)

### Test cases, multiplication ###

Multiplication did not work on the first attempts.  After some bumbling around and toying with the multiplication function and looking at registers, I decided that test cases were needed to test edge cases.  Because my algorithm checks for zeros, ones, and has interesting quirks on even numbers and ones in the halving operand.  

So I built a set of test cases that would test the edge cases.  I kept the cases simple, limiting the number of iterations needed.  My thought was that I could make a simple set of test cases to test edge cases.  If it passed all the tests, then the multiplication algorithm should function properly on larger, more complex operations.  

The test cases were:

1. 0x00, 0x33, 0x0F, 0x55, expected result was 0x00
2. 0x0F, 0x33, 0x00, 0x55, expected result was 0x00
3. 0x0F, 0x33, 0x01, 0x55, expected result was 0x0F
4. 0x01, 0x33, 0x0F, 0x55, expected result was 0x0F
5. 0x0F, 0x33, 0x08, 0x55, expected result was 0x78



### Results of tests and refinements ###

At first, the only test case that failed was test case number 5.  All the other test cases passed, shown below. 
 
Results shown below:

1. Test case 1:
![Memory Dump](images/multtc1.PNG)

2. Test case 2: 
![Memory Dump](images/multtc2.PNG)

3. Test case 2:
![Memory Dump](images/multtc3.PNG)

4. Test case 2:
![Memory Dump](images/multtc4.PNG)

Test case 5 did not initially pass.  The error I was getting was the result of the halving operand not catching on 0x01 and ending the operation.  Instead, the program would continue on, and the halving operand would just keep shifting bits, leading to an overflow error.  The program would just keep overflowing and looping.  

The error was being caused by the checks being in the wrong order, or just missing.  The program would shift bits before or without checking to see if the halving operand was equal to 0x01.  The program would bit shift past to 0x00, and thus would never reach one.  Then it would overflow and start pulling in overflow bits and become an extremely large negative number.  The program would never complete.  In my test case, the program would not trap the CPU, and the number in register would eventually end up being 0xFFFF.  

The problem was being caused by the label for looping being in the wrong place.  It was placed after the proper checks, so they weren't getting executed, and thus not catching the 0x01 in the halving operand.  The easy fix was to shift the label to the top of the operation.  The reason some of the checks are run twice in an iteration is to catch the number before it gets the chance to bit shift again and change the output.  It's less efficient, but once I got it working, I opted against any more alterations and messing up the correct operation of the software.

Test case 5 shown working below: 

![Memory Dump](images/multtc5.PNG)   

Provided A functionality shown working below:

![Memory Dump](images/afunctionality.PNG)

When I originally tested the provided test case, the first multiplications would work, but in a case starting with an even halving operand, the program would blow right by 0x01, and only have half of the result written into memory.  Fixing this with the 5th test case, testing for edge cases, solved a lot of my program, as I could actually see what was going on.  

## Observations and Conclusions ##

### Calculator and ease of coding ###

The calculator is pretty easy to implement.  The hard part is multiplication and error checking.  However, the vast majority of the function is done by comparisons and bit checking.  From op code comparisons, to bit checking for odd/even operands, or even just comparisons with 0xFF and 0x00.  Understanding these comparisons and the flags that are set, and then which jumps are triggered by which flags.  Taking advantage of JHS and JN, as well as JZ, makes jumping on the right conditions easier to do.  

### Multiplying without multiplier circuit ###

Multiplying without a dedicated operation on the ALU is requires more effort on the part of the programmer, rather than a more expensive CPU.  Fancy algorithms to simulate the results are workable, if much less efficient and more prone to error than a dedicated circuit.  

It's ugly, complex, prone to issues, but it's better than nothing.  More importantly, it works.  

### Olog(n), a quick overview of speed ###

The clever use of bit-shifting and other algorithms can result in the same result as multiplication.  The biggest difference is the speed at which the program executes.  The fastest option would be a dedicated circuit, and would ideally only take one circuit process to execute.  This is not an option on the MSP430, thus the need for an algorithm.  A simple coding approach would be to do looped addition.  By simply looping and adding an operand to itself the number of times of the other operand, a matching result to multiplication could be produced.  However, this is a O(n) approach, and could range from 0x0000 iterations to 0xFFFF cycles to complete.  It doesn't scale logarithmically with the length of the operands and is terribly inefficient.  

My approach was more efficient.  The russian peasant multiplication algorithm shifts bits to work, and theoretically would max out at about 16 iterations. This is an Olog(n) approach, and is exponentially more efficient.  This was the reason I chose this approach.  It's more efficient, takes less cycles to complete, and is manageable to do by hand and check the results step by step.

#### Documentation ####

EI With Capt. Falkinburg, Trimble

Got help from C2C Griffith with regards to loops, memory addressing, and variable naming and referencing
