#include <msp430.h> 
#include "Robot.h"
#include "ultraSound.h"

float distance = 120.0;
char position = 1;
long distLeft = 120;
long distRight = 120;
long distance1 = 120;
long distance2 = 120;
long distance3 = 120;
long distance4 = 120;
long distance5 = 120;

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    initRobot();

    __delay_cycles(500000);
	TA1CCR1 = 800;                // set duty cycle to 0
	TA1CCR2 = 1000;                // set duty cycle to 0
	__delay_cycles(500000);
	TA1CCR1 = 1000;
	TA1CCR2 = 950;
	__delay_cycles(400000);
	TA1CCR1 = 0;                // set duty cycle to 0
	TA1CCR2 = 0;                // set duty cycle to 0
	tankLeft();
	TA1CCR1 = 1000;                // set duty cycle to 0
	TA1CCR2 = 900;                // set duty cycle to 0
	__delay_cycles(300000);
	TA1CCR1 = 1000;
	TA1CCR2 = 600;
	__delay_cycles(200000);
	tankRight();
	TA1CCR1 = 1000;
	TA1CCR2 = 800;
	__delay_cycles(600000);
	TA1CCR1 = 1000;
	TA1CCR2 = 950;
	__delay_cycles(400000);
	TA1CCR1 = 400;
	TA1CCR2 = 1000;
	__delay_cycles(700000);
	TA1CCR1 = 0;
	TA1CCR2 = 0;



    /*
    sonicSetup();

    position = sensorCenter(position);

    while(1){
        sensorInit();
        distance1 = sensorPing();
        sensorInit();
        distance2= sensorPing();
        sensorInit();
        distance3 = sensorPing();
        sensorInit();
        distance4= sensorPing();
        sensorInit();
        distance5 = sensorPing();

        distance = (distance1 + distance2 + distance3 + distance4 + distance5)/5.0;
        //distance = (distance1 + distance2)/2;

        setForward();

        if (distance < 2.5){
        	allStop();

        	position = sensorLeft(position);
            sensorInit();
            distLeft = sensorPing();

            position = sensorRight(position);
            sensorInit();
            distRight = sensorPing();

            if((distRight - 15) > distLeft){  // +7 to baias towards left turn
            	tankRight();
            }
            else{
            	tankLeft();
            }
            position = sensorCenter(position);
            distance = 120;
        }
    }
	*/
	return 0;
}
