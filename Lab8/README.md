## Lab 8: I ran out of Star Wars names, so I call it: TOKYO DRIFT 8 ##

### Objectives ###

The Objectives of the lab were to:

* For required functionality: Get the robot through door 1 of the maze (image shown later) using the ultrasonic sensor.
* For B functionality: Get the robot through door 2 of the maze using the ultrasonic sensor.
* For A functionality: Get the robot through door 3 using the ultrasonic sensor.
* Bonus: Run the robot from door 3 to the start of the maze using the ultrasonic sensor
* SUPER Bonus: Race from the start of the maze to door 3, as fast as possible.  Use of the ultrasonic sensor is not required.  

### Purpose ###

The purpose of the lab was to learn how to properly work with a servo (SV92R) and ultrasonic sonar device (HC-SR04).

## Prelab ##

The prelab was solely to create a rough draft of the algorithm, and to start thinking about the algorithm and what to actually code onto the robot.  

### Algorithm ###

* if forward free, move forward
* once wall <= 5 inches, stop
* scan left and right
* turn to whichever side has more free space
* loop

I will have to re-write my movement stuff from lab 5/6 to move like a tank, rather than one wheel at a time.  Should be quick.  Good thing my library makes this easier for me to do.

One of the biggest challenges will be moving the robot and having the sensor pinging constantly without using delays (and smacking into walls).  Perhaps pinging constantly in a while loop with the distance as the exit condition of the loop.  Slow and steady wins the race here, racing around could cause problems and a lot of robots smacking into walls.

## Functionality ##

### A Functionality ###

Before I explain A functionality, I note that I attempted A functionality directly rather than go for required or B functionality first.  

The challenges here are the precision of the robot.  While the sensor is accurate, sometimes individual readings are not.  To combat this, I take multiple readings with the sensor and average them.  Doing this to a number that is stored as a float increases accuracy.  

At first I start by importing the header files for the robot movement from lab 6, and the ultrasonic sensor from lab 7. Please refer to those lab reports for information on those functions.  

Then I set up a simple never-ending while loop to contain the algorithm.  The algorithm starts by pinging 5 times to build the average distance (an accurate distance).  The robot then sets the motors duty cycle to move forward, without using any delays (as to not slam into a wall while waiting for the delay to end).  If the sensor detects a wall arbitrarily close (i.e. 2.5 inches) the robot sets the duty cycle for the motors to 0% (stops the robot).  The robot then senses to the left and right, and whichever has more free space the robot will turn 90 degrees in place in the direction, and center the sensor, and repeat.  This successfully got me A functionality.  

Code is shown below:

![Code for A](images/code1.PNG)
![Code for A](images/code2.PNG)

### Bonus Functionality ###

This was fun.  Using an algorithm that successfully completed A functionality, the robot must run backwards through the maze using the same exact code.

The only alteration needed was on line that reads: if((distRight - 15) > distLeft).  I added in the -15 to give some assistance to the right turn, as the robot had a tendency to stay in the open space part of the maze due to the positioning of the wall on the left being closer than the wall on the right at the point where the robot needed to turn left, not right.  Putting the robot at door 3 and turning it on produced the desired result, and I picked the robot up at the entrance to the maze.

### SUPER Bonus Functionality ###

This was significantly more fun.  In this part, the use of the ultrasonic sensor was not required.  I hard-coded (i.e. manually set TACCR values and delays) the run through the maze for the quickest run possible.  

Shown below is the code:  

![Code for A](images/manualrun.PNG)

This code uses the highest possible motor settings on several occasions in the run.  The idea here is speed.  The robot runs forward, turns left, provided a burst of power, before turning past 90 degrees to the right, and then correcting its course to exit the door.  The robot tends to drift during this course due to the high power to the motors and its own momentum.  Final official time was 3.34 seconds, coming in 3rd for the section and netting me 10 extra bonus points total.

## Testing and Debugging ##

Ah, yes.  The fun stuff.  The Robot is notoriously difficult to work with, and small changes can result in large changes in behavior in the maze.  

First, I had to change the distance to a float to get the proper precision and math out of the distance sensor.  Otherwise, 2 inches was too short a stopping distance, and 3 inches was too much.  2.5 was a happy medium and the added precision gave a larger range of discrete options for the if statement to use.  

Second, I had to add a bias to the right turn.  In the maze, going backwards for the bonus functionality, the right wall would be further than the left wall at a spot where the robot would need to go left.  By subtracting the difference in the wall distances at that point from the right side, the robot would turn left, but still work for A functionality (same exact algorithm!) and work for bonus functionality.  

Finally, the SUPER bonus functionality.  As of the night before the actual competition run, the robot had an unofficial time of 3.4 seconds.  I built the speed run a segment at a time.  There was some tweaking needed.  The robot has motors that arent perfectly matched, so tweaking was important.  The left motor was slightly weaker than the right, so that needed to be accounted for.  Building the run a TA1CCR1/2 pair a time, I was able to test the run bit by bit.  Due to the motor mismatch, the robot needs to start with a 5-10 degree offset at the start to run straight, and the robot oversteers on the >90 degree turn, and must correct before the wall to make it through the door and complete the run.

## Observations and Conclusions ##

This stuff is hard, but is the practical culmination of a semester's worth of work with a microcontroller.  Building a robot to accomplish a practical task was rewarding, if complex.  It's nice to finally understand what I am doing with the MSP430.

### Documentation ###

None.  All functionality was provided by prior labs, and just needed to be combined.