//-----------------------------------------------------------------
// Name:	Rogovin
// File:	ultrasound sensor
// Date:	Fall 2015
// Purp:	Move and use servo/ultrasound

//Documentation:
//-----------------------------------------------------------------

#include <msp430.h>


void sensorInit(){
	P1OUT |= BIT4;
	__delay_cycles(850);
	P1OUT &= ~BIT4;
}
char sensorLeft(pos){
	pos = 1;
	TA0CCR1 = 2500;
	__delay_cycles(1000000);
	TA0CCR1 = 0;
	return pos;
}

long sensorPing(){

	TA0CCR0 = 0xFFFF;     // needs to be high to get larger distances

	P2IN = 0;
	int time = 0;

	int length;

	while((P2IN & BIT3) == 0){}
	TAR = 0;

	while((P2IN & BIT3) != 0){}
	time = TAR;
	length = 0;
	length = ((time));

	TA0CCR0 = 5000;       // reset to previous value for servo control

	return length/146;    //change 146 to 58 for output in cm
}

char sensorCenter(pos){
	pos = 0;
	TA0CCR1 = 3560;
	__delay_cycles(1000000);
	TA0CCR1 = 0;
	return pos;
}
char sensorRight(pos){
	pos = 2;
	TA0CCR1 = 4500;
	__delay_cycles(1000000);
	TA0CCR1 = 0;
	return pos;
}

void sonicSetup(){
	P1DIR |= BIT2|BIT4;
	P1SEL |= BIT2;
	TA0CTL |= TASSEL_2|MC_1|ID_0;

	TA0CCR0 = 5000;
	TA0CCR1 = 0;
	TA0CCTL1 |= OUTMOD_3;
}

