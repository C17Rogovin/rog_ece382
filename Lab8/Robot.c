//-----------------------------------------------------------------
// Name:	Rogovin
// File:	Robot functions
// Date:	Fall 2015
// Purp:	Contain robot driving functions

//Documentation: Lots of EI, worked with C2C Chiriac most of saturday figuring that all out
//-----------------------------------------------------------------

#include <msp430.h>
void initRobot(){
	WDTCTL = WDTPW|WDTHOLD;       // stop the watchdog timer NOT NEEDED, ALREADY DONE IN START5


	P2DIR |= (BIT1|BIT4);           // TA1CCR1 on P2.1, TA1CCR2 on P2.3
	P2SEL |= (BIT1|BIT4);           // P2.1, P2.4 on

	TA1CTL |= TASSEL_2|MC_1|ID_0; // configure for SMCLK
    TA1CCR0 = 1000;               // set signal period to 1000 clock cycles (~1 millisecond)
	TA1CCR1 = 0;                  // set duty cycle to 0
	TA1CCR2 = 0;                  // set duty cycle to 0
	TA1CCTL1 |= OUTMOD_3;         // set TACCTL1 to Set / Reset mode
	TA1CCTL2 |= OUTMOD_3;         // set TACCTL1 to Set / Reset mode

}

void aheadFull(){
	TA1CCR1 = 0;                // set duty cycle to 0
	TA1CCR2 = 0;                // set duty cycle to 0
	//__delay_cycles(8*1000000);
	TA1CCR1 = 520;              // set duty cycle to 50%
	TA1CCR2 = 500;              // set duty cycle to 50%
	__delay_cycles(8*900000);
	TA1CCR1 = 0;                // set duty cycle to 50%
	TA1CCR2 = 0;                // set duty cycle to 10%
	__delay_cycles(1000000);
}

void driftToStarboard(){
	TA1CCR1 = 0;                // set duty cycle to 50%
	TA1CCR2 = 0;                // set duty cycle to 10%
	//__delay_cycles(8*1000000);
	//TA1CCR1 = 500;              // set duty cycle to 50%
	__delay_cycles(8*500000);
	TA1CCR1 = 0;                // set duty cycle to 0%
	__delay_cycles(1000000);
}

void hardToStarboard(){
	TA1CCR1 = 0;                // set duty cycle to 0%
	TA1CCR2 = 0;                // set duty cycle to 0%
	//__delay_cycles(8*1000000);
	TA1CCR1 = 500;              // set duty cycle to 50%
	__delay_cycles(8*800000);
	TA1CCR1 = 0;                // set duty cycle to 0%
	__delay_cycles(1000000);
}

void driftToPort(){
	TA1CCR1 = 0;                // set duty cycle to 50%
	TA1CCR2 = 0;                // set duty cycle to 10%
	//__delay_cycles(8*1000000);
	TA1CCR2 = 500;              // set duty cycle to 10%
	__delay_cycles(8*500000);
	TA1CCR2 = 0;                // set duty cycle to 0%
	__delay_cycles(1000000);
}

void hardToPort(){
	TA1CCR1 = 0;                // set duty cycle to 0%
	TA1CCR2 = 0;                // set duty cycle to 0%
	//__delay_cycles(8*1000000);
	TA1CCR2 = 500;              // set duty cycle to 10%
	__delay_cycles(8*750000);
	TA1CCR2 = 0;                // set duty cycle to 0%
	__delay_cycles(1000000);
}

void leftReverse(){
	P2DIR &= ~(BIT1);         //turn off forward
	P2SEL &= ~(BIT1);

	P2DIR |= (BIT2);           // TA1CCR1 on P2.2  ENGAGE REVERSE MODE
	P2SEL |= (BIT2);           // TA1CCR1 on P2.2

}

void rightReverse(){
	P2DIR &= ~(BIT4);
	P2SEL &= ~(BIT4);

	P2DIR |= (BIT5);           // TA1CCR2 on P2.5
	P2SEL |= (BIT5);           // TA1CCR2 on P2.5

}

void leftFoward(){

	P2DIR &= ~(BIT2);           // TA1CCR1 on P2.1
	P2SEL &= ~(BIT2);           // TA1CCR1 on P2.1

	P2DIR |= (BIT1);           // TA1CCR1 on P2.1
	P2SEL |= (BIT1);           // TA1CCR1 on P2.1

}

void rightFoward(){
	P2DIR &= ~(BIT5);           // TA1CCR2 on P2.4    TURN OFF REVERSE
	P2SEL &= ~(BIT5);           // TA1CCR2 on P2.4

	P2DIR |= (BIT4);           // TA1CCR2 on P2.4     TURN ON FORWARD
	P2SEL |= (BIT4);           // TA1CCR2 on P2.4

}

void fullReverse(){
	TA1CCR1 = 0;                // set duty cycle to 0
	TA1CCR2 = 0;                // set duty cycle to 0

	//__delay_cycles(8*1000000);

	leftReverse();
	rightReverse();

	TA1CCR1 = 515;                // set duty cycle to 50%
	TA1CCR2 = 500;                // set duty cycle to 50%

	__delay_cycles(8*1000000);

	TA1CCR1 = 0;                // set duty cycle to 0
	TA1CCR2 = 0;                // set duty cycle to 0

	//__delay_cycles(8*1000000);

	leftFoward();
	rightFoward();

}

void setForward(){
	TA1CCR1 = 0;                // set duty cycle to 0
	TA1CCR2 = 0;                // set duty cycle to 0
	//__delay_cycles(8*1000000);
	TA1CCR1 = 410;              // set duty cycle to 50%
	TA1CCR2 = 400;              // set duty cycle to 50%

}

void allStop(){
	TA1CCR1 = 0;                // set duty cycle to 0
	TA1CCR2 = 0;                // set duty cycle to 0
	leftReverse();
	rightReverse();
	TA1CCR1 = 205;                // set duty cycle to 0
	TA1CCR2 = 200;                // set duty cycle to 0
	__delay_cycles(100000);
	TA1CCR1 = 0;                // set duty cycle to 0
	TA1CCR2 = 0;                // set duty cycle to 0
	leftFoward();
	rightFoward();
}


void tankLeft(){
	leftReverse();
	rightFoward();

	TA1CCR1 = 410;                // set duty cycle to 0
	TA1CCR2 = 400;                // set duty cycle to 0

	__delay_cycles(550000);
	TA1CCR1 = 0;                // set duty cycle to 0
	TA1CCR2 = 0;                // set duty cycle to 0
	leftFoward();

}

void tankRight(){
	rightReverse();
	leftFoward();

	TA1CCR1 = 410;                // set duty cycle to 0
	TA1CCR2 = 385;                // set duty cycle to 0
	__delay_cycles(530000.0/2.0);     //changed for comp
	TA1CCR1 = 0;                // set duty cycle to 0
	TA1CCR2 = 0;                // set duty cycle to 0
	rightFoward();
}

