## HC-SR04 Ultrasonic Sensor Library, using SG92R Servo ##

### Tech Doc ###

The technical documentation is provided in the library.  Technical documents state:

* 5VDC Power supply needed
* Draws less than 2mA
* Effective distance is from 2cm-500cm (1 inch to ~15 ft)
* Resolution of .3cm

Ultrasonic ranging is accomplished by sending a 10mS pulse to the trigger connection, triggering a ranging pulse.  The response is a high pulse proportional to the distance.  Testing and regression found this relationship to be:

TAR counts = 146 * inches + 62

TAR was set to an 8MHz clock, and ranging was personally tested and was shown to be accurate to beyond 10 feet.

### Files/Functions included ###

Two files were included:  ultraSound.c and ultraSound.h

Contained in these files are the following functions:

* void sensorInit();
* char sensorCenter(char position);
* char sensorLeft(char position);
* char sensorRight(char position);
* long sensorPing();
* void sonicSetup();

### How to Use ###

In order to use the ultrasonic sensor for rangefinding, do the following:

All code was designed for use with an MSP430G2553 board.  Trigger was set on P1.4, Echo on P2.3, and servo on P1.2.

Run sonicSetup() before calling any of the other functions.  It is recommended that this function not be called more than once, as it wastes time.  

To move a sensor to a specific direction, call sensorCenter(), sensorLeft(), and sensorRight().  Each function takes the current position as an input (of type char), and each of these functions returns a char which can be used to track the current location, listed below.

* Left: 1
* Right: 2
* Center: 0

To get the distance in inches, call sensorInit() to trigger a pulse, and then sensorPing() to read the pulse.  sensorPing() will then return the distance, in inches.  To edit to use cm, change 146 to 58 on line 45 of ultraSound.c